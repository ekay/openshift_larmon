from flask import Flask, request, render_template, session, redirect
from datetime import datetime
import json
import yaml
import base64
import requests

print(f"hello {__name__} file {__file__}")
app = Flask(__name__)
app.secret_key = 'some secret key'
print(f"hello {__name__} after creating {app}")

project_id="117251"
gitlab_url="https://gitlab.cern.ch/api/v4"
trigger_token="3efc6af3ab725f88a9cf069bb5da82"
query_token='tRnsqWacneapsmVzV1PW'
variables_id="USER_PARAMETERS"

@app.route('/')
@app.route('/index')
def index():
    print(f"rendering index, request path [{request.path}]")
    return render_template('index.html')

@app.route('/start_job')
def start_job():
    print(f"navigating to start_job, request path [{request.path}]")
    return render_template('start_job.html')

@app.route('/results')
def results():
    print(f"rendering results, request path [{request.path}]")
    pipelines = get_all_pipelines_as_json()
    return render_template('results.html', job_id=get_session_value('job_id'), pipelines=pipelines, \
      page_refreshed_ts=datetime.now().strftime("%d/%m/%Y %H:%M:%S"), latest_report_url=get_session_value('latest_report_url'))

@app.route('/expert')
def expert():
    print(f"rendering expert, request path [{request.path}]")
    return render_template('expert.html', notebook_branch_name=get_session_value('notebook_branch_name'), \
      latest_report_url=get_session_value('latest_report_url')) 

def get_session_value(key):
    session_defaults = {
      'job_id': 'None',
      'notebook_branch_name': 'master',
      'latest_report_url': 'https://da-ics.web.cern.ch/reports/encvcl/report.pdf'
    }
    if not session.get(key):
        session[key] = session_defaults.get(key)
    return  session[key]

def http_response_content_to_json(http_response):
    content_bytes = http_response._content
    content_as_string = content_bytes.decode('utf8')
    content_as_json = json.loads(content_as_string)
    #print(f"response from trigger {content_as_json}")
    return content_as_json

def get_all_pipelines_as_json():
    target_url=f'https://gitlab.cern.ch/api/v4/projects/{project_id}/pipelines'
    response = requests.get(target_url, headers={'PRIVATE-TOKEN':f'{query_token}'})
    print(f"piplines GET:[{target_url}], response:[{response}]")
    return http_response_content_to_json(response)

def trigger_job(user_parameters):
    print(f"trigger_job called with [{user_parameters}]")
    user_parameters_string = user_parameters.decode('ascii')
    notebook_branch_name=get_session_value('notebook_branch_name')
    target_url=f'{gitlab_url}/projects/{project_id}/ref/{notebook_branch_name}/trigger/pipeline?token={trigger_token}&variables[{variables_id}]={user_parameters_string}'
    response = requests.post(target_url)
    print(f"trigger  POST:[{target_url}], response:[{response}]")
    return http_response_content_to_json(response)

@app.route('/analysis_parameters_submit', methods=['GET'])
def analysis_parameters():
    print(f"analysis_parameters_submit: [{request.args}], triggering test job")
    params_yaml = yaml.dump(request.args.to_dict(flat=False), encoding=('ascii'))
    params_encoded = base64.b64encode(params_yaml)
    print(f"params_encoded [{params_encoded}]")
    session['job_id'] = trigger_job(params_encoded)['id']
    print(f"moving to results page...")
    return redirect('/results')

@app.route('/expert_parameters_submit', methods=['GET'])
def expert_parameters():
    session['notebook_branch_name']=request.args['notebook_branch_name']
    session['latest_report_url']=request.args['latest_report_url']
    print(f"expert_parameters_submit: [{request.args}], notebook_branch_name [{get_session_value('notebook_branch_name')}] report URL [{get_session_value('latest_report_url')}]")
    return redirect('/expert')
